class ProductAttachment < ActiveRecord::Base

	mount_uploader :attachment, AttachmentUploader
	belongs_to :product
	validate :file_size


	def file_size
		logger.info "**************FILE SIZE UPLOADE #{attachment.file.size}**************"
		if attachment.file.size > 15000000
			errors.add(:attachment, "You cannot upload a file greater than 15MB")
		end
	end
end
