class HomeController < ApplicationController

	skip_before_action :authenticate_user!, only: [:index, :home_products]

	def index

		# @products = Product.random_active.limit(6).includes(:product_attachments).includes(:product_reviews)
		@products = Product.public_active.order('random()').limit(6).includes(:product_attachments).includes(:product_reviews)

		@products.each do |product|
			if product.currency != session[:currency]
				if session["currency-convert-#{product.currency}"].to_f == 0.0
					rate = 1.0	
				else
					rate = session["currency-convert-#{session[:currency]}"].to_f / session["currency-convert-#{product.currency}"].to_f
				end
	      
	    else
	      rate = 1.0
	    end

			product.price_with_currency = (product.price_cents * rate / 100).round(2)
			product.current_currency = session[:currency]
		end
		@product_attachments = ProductAttachment.all 
		#logger.info "status=Fetching Home Product image=#{@products.product_attachments.try(:first).attachment}" 
		@cref = Creference.all
		@cref = @cref.group_by{|cc| cc["name"]}
		

		@countries = Product.joins(:user).distinct.where("country is not null and country <> ''").where(:users => {:merchant_status => 1}).pluck(:country)

		@cities = Product.joins(:user).distinct.where("city is not null and city <> ''").where(:users => {:merchant_status => 1}).pluck(:city)

		@state = Product.joins(:user).distinct.where("state is not null and state <> ''").where(:users => {:merchant_status => 1}).pluck(:state)
		
		@countries_cities = Product.joins(:user)
                           .where.not(country: [nil, ''])
                           .where(users: {merchant_status: 1})
                           .group(:country, :city)
                           .order("country!= 'Malaysia'")
                           .select(:country, :city)
                           .as_json
		#binding.pry
		gon.countries_cities = @countries_cities
		cchs = @countries_cities.group_by{|cc| cc["country"]}
		#@country_cities_hash = Hash[*cchs]
		#binding.pry
		# process for case insentive
		# @countries.each do |country|
		# 	country = country.split.map(&:capitalize).join(' ')
		# end

		@countries.map! {|country| country.split.map(&:capitalize).join(' ')}

		# @cities.each do |city|
		# 	city = city.split.map(&:capitalize).join(' ')
		# end

		@cities.map! {|city| city.split.map(&:capitalize).join(' ')}
		@state.map! {|state| state.split.map(&:capitalize).join(' ')}

		@countries = @countries.uniq
		@cities = @cities.uniq
		@state = @state.uniq
		gon.cref = @cref
		gon.state = @state
		gon.search_location_country = @countries
		search_location_list = @countries + @cities + @state + @cref.keys
		gon.search_location_list = search_location_list.uniq
		gon.search_interests = ProductCategory.select(:name).pluck(:name)
		@search_location_list = @countries + @cities
		@search_location_country = cchs
		# gon.home_products = @products

		# count per city
		@count_per_city = {}
		@cities.each do |city|
			@count_per_city[city] = Product.public_active.where("lower(city) LIKE ? or lower(country) like ?", "%#{city.downcase}%", "%#{city.downcase}%").count
		end

		@products.each do |product|
			product.set_extra_attributes
		end

	end

	def home_products
		city = params[:city]
		if city.downcase == 'all cities'
			@products = Product.public_active.order('random()').limit(6).includes(:product_attachments).includes(:product_reviews)
		else
			@products = Product.public_active.order('random()').where("lower(city) LIKE ? or lower(country) like ?", "%#{params[:city].downcase}%", "%#{params[:city].downcase}%").limit(6)
		end
		
		@products.each do |product|
			if product.currency != session[:currency]
	      rate = session["currency-convert-#{session[:currency]}"].to_f / session["currency-convert-#{product.currency}"].to_f
	    else
	      rate = 1.0
	    end

			product.price_with_currency = (product.price_cents * rate / 100).round(2)
			product.current_currency = session[:currency]
		end

		@products.each do |product|
			product.set_extra_attributes
		end
		render :layout => false
	end

	# def search
	# 	render :template => "products/result"
	# end

end
